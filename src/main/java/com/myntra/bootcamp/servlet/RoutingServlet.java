package com.myntra.bootcamp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myntra.bootcamp.servlet.handlers.OrderHandler;
import com.myntra.bootcamp.servlet.handlers.UserHandler;

public class RoutingServlet extends HttpServlet {

	private static final long serialVersionUID = -966405302967357572L;

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String uri = request.getRequestURI();
        if(uri.contains("fetch-existing-user")) {
            UserHandler.doGet(request, response);
        }
        else if(uri.contains("create-new-user")) {
            UserHandler.doPost(request, response);
        }
        else if(uri.contains("remove-order-byid")) {
            OrderHandler.doDelete(request, response);
        }
        else if(uri.contains("create-new-order")) {
            OrderHandler.doPost(request, response);
        }
        else {
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<body>");
            out.println("<h1>This URL is not supported</h1>");
            out.println("</body>");
            out.println("</html>"); 
        }
    }
    
}
